class CreatePlayers < ActiveRecord::Migration[5.2]
  def change
    create_table :players do |t|
      t.string :fields
      t.string :nickname
      t.string :rank
      t.string :charizma
      t.string :wisdom

      t.timestamps
    end
  end
end
